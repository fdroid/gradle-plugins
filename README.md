
# F-Droid Gradle Plugins

This is a collection of plugins to let _fdroid_ tools run things in
_gradle_ in order to get information from an app's source repo.  This
is often necessary since gradle does a lot of dynamic configuration,
so parsing the _build.gradle_ files is not enough to get the full
configuration.

This repo is meant to be cloned into _~/.gradle_ and used from there:

```console
$ mkdir ~/.gradle
$ git clone -C ~/.gradle https://gitlab.com/fdroid/gradle-plugins.git fdroid
$ cd /path/to/app/source
$ ./gradlew --init-script /home/hans/.gradle/fdroid/triplet.gradle
$ cat .fdroid.triplet.json 
[{"defaultConfig":{"applicationId":"net.gsantner.markor","applicationIdSuffix":null,"versionName":"2.2.7","versionCode":109}},{"flavorAtest":{"applicationId":"net.gsantner.markor_test","applicationIdSuffix":null,"versionName":"200226","versionCode":200226}},{"flavorDefault":{"applicationId":null,"applicationIdSuffix":null,"versionName":null,"versionCode":null}},{"flavorGplay":{"applicationId":null,"applicationIdSuffix":null,"versionName":null,"versionCode":null}}]
```
